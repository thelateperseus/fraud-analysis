package nathan;

public interface Constants {
    public static final String CARD_TRANSACTION_TOPIC = "CardTransaction";
    public static final String MERCHANT_TOPIC = "Merchant";
    public static final String FRAUD_ALERT_TOPIC = "FraudAlert";

    public static final String SERVER_HOSTNAME = "172.25.182.62";
}
