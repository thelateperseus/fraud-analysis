package nathan.producer;

import static nathan.Constants.CARD_TRANSACTION_TOPIC;
import static nathan.Constants.SERVER_HOSTNAME;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import nathan.model.CardTransaction;

public class CardTransactionProducer {

    private static Producer<Long, CardTransaction> producer;

    public static void main(final String[] args) throws Exception {

        final Properties props = new Properties();
        // hardcoding the Kafka server URI for this example
        props.put("bootstrap.servers", SERVER_HOSTNAME + ":9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("key.serializer", "org.apache.kafka.common.serialization.LongSerializer");
        props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        props.put("schema.registry.url", "http://" + SERVER_HOSTNAME + ":8081");

        producer = new KafkaProducer<>(props);

        send(new CardTransaction(
            1L,
            "Test Merchant",
            "1234",
            "4444333322221111",
            Double.parseDouble("129.99"),
            "8.8.8.8",
            "51"));

        send(new CardTransaction(
            2L,
            "Test Merchant",
            "1234",
            "2221112221112222",
            Double.parseDouble("222.00"),
            "8.8.8.8",
            "08"));

        send(new CardTransaction(
            3L,
            "Test Merchant",
            "1234",
            "4444333322221111",
            Double.parseDouble("129.99"),
            "8.8.8.8",
            "05"));
    }

    private static void send(final CardTransaction txn) throws Exception {
        final ProducerRecord<Long, CardTransaction> record =
            new ProducerRecord<>(CARD_TRANSACTION_TOPIC, txn.getId(), txn);
        producer.send(record).get();
    }
}
