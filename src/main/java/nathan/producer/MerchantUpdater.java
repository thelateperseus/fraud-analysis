package nathan.producer;

import static nathan.Constants.MERCHANT_TOPIC;
import static nathan.Constants.SERVER_HOSTNAME;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import nathan.model.Merchant;

public class MerchantUpdater {

    private static Producer<String, Merchant> producer;

    public static void main(final String[] args) throws Exception {

        final Properties props = new Properties();
        // hardcoding the Kafka server URI for this example
        props.put("bootstrap.servers", SERVER_HOSTNAME + ":9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        props.put("schema.registry.url", "http://" + SERVER_HOSTNAME + ":8081");

        producer = new KafkaProducer<>(props);

        send(new Merchant("1234", "Test Merchant", 3L));
    }

    private static void send(final Merchant merchant) throws Exception {
        final ProducerRecord<String, Merchant> record =
            new ProducerRecord<>(MERCHANT_TOPIC, merchant.getId(), merchant);
        producer.send(record).get();
    }
}
