package nathan.rest;

public class MerchantDto {
    private String id;
    private String name;
    private long alertThreshold;

    public MerchantDto(final String id, final String name, final long alertThreshold) {
        this.id = id;
        this.name = name;
        this.alertThreshold = alertThreshold;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public long getAlertThreshold() {
        return alertThreshold;
    }

    public void setAlertThreshold(final long alertThreshold) {
        this.alertThreshold = alertThreshold;
    }
}
