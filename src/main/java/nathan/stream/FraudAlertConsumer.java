package nathan.stream;

import static nathan.Constants.FRAUD_ALERT_TOPIC;
import static nathan.Constants.SERVER_HOSTNAME;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import nathan.model.FraudAlert;

public class FraudAlertConsumer {

    public static void main(final String[] args) throws Exception {
        final String schemaRegistryUrl = "http://" + SERVER_HOSTNAME + ":8081";

        final Properties consumerConfiguration = new Properties();
        consumerConfiguration.put(ConsumerConfig.CLIENT_ID_CONFIG, "FraudAlertConsumer");
        consumerConfiguration.put(ConsumerConfig.GROUP_ID_CONFIG, "FraudAlertConsumer");
        consumerConfiguration.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVER_HOSTNAME + ":9092");
        consumerConfiguration.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
        consumerConfiguration.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerConfiguration.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        consumerConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        final KafkaConsumer<String, FraudAlert> consumer =
            new KafkaConsumer<>(consumerConfiguration);

        consumer.subscribe(Arrays.asList(args.length == 0 ? FRAUD_ALERT_TOPIC : args[0]));

        while (true) {
            final ConsumerRecords<String, FraudAlert> records = consumer.poll(Duration.ofSeconds(1));
            records.forEach(r -> System.out.println(r.value()));
        }
    }

}
