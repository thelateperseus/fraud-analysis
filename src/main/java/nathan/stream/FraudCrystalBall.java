package nathan.stream;

import static nathan.Constants.CARD_TRANSACTION_TOPIC;
import static nathan.Constants.FRAUD_ALERT_TOPIC;
import static nathan.Constants.MERCHANT_TOPIC;
import static nathan.Constants.SERVER_HOSTNAME;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.HostInfo;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import nathan.model.CardTransaction;
import nathan.model.FraudAlert;
import nathan.model.Merchant;
import nathan.model.MerchantIpAddress;
import nathan.rest.FraudRestService;

public class FraudCrystalBall {

    public static void main(final String[] args) throws Exception {
        final String schemaRegistryUrl = "http://" + SERVER_HOSTNAME + ":8081";

        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "FraudCrystalBall");
        streamsConfiguration.put(StreamsConfig.CLIENT_ID_CONFIG, "FraudCrystalBall");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, SERVER_HOSTNAME + ":9092");
        streamsConfiguration.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
        streamsConfiguration.put(StreamsConfig.APPLICATION_SERVER_CONFIG, "localhost:7070");
        // Specify default (de)serializers for record keys and for record values.
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, "kafka-streams");
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 500);
        // Disable caching so that all count updates are propagated
        streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        final Serde<Long> longSerde = Serdes.Long();
        final Serde<String> stringSerde = Serdes.String();

        final Map<String, String> serdeConfig = Collections.singletonMap(
            AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);

        final SpecificAvroSerde<MerchantIpAddress> merchantIpAddressSerde = new SpecificAvroSerde<>();
        merchantIpAddressSerde.configure(serdeConfig, true);
        final SpecificAvroSerde<CardTransaction> cardTransactionSerde = new SpecificAvroSerde<>();
        cardTransactionSerde.configure(serdeConfig, false);
        final SpecificAvroSerde<FraudAlert> fraudAlertSerde = new SpecificAvroSerde<>();
        fraudAlertSerde.configure(serdeConfig, false);

        final StreamsBuilder builder = new StreamsBuilder();

        // read the source stream
        final KStream<Long, CardTransaction> transactions =
            builder.stream(CARD_TRANSACTION_TOPIC, Consumed.with(longSerde, cardTransactionSerde));
        final GlobalKTable<String, Merchant> merchants =
            builder.globalTable(MERCHANT_TOPIC, Materialized.as("Merchants"));

        transactions.print(Printed.<Long, CardTransaction>toSysOut().withLabel("transactions"));

        // aggregate the new feed counts of by user
        final KStream<Windowed<MerchantIpAddress>, Long> counts = transactions
            // remove declined transactions
            .filter((key, value) -> !Arrays.asList("00", "08").contains(value.getResponseCode()))
            // group by the merchant id and ip address
            .groupBy(
                (key, value) -> new MerchantIpAddress(value.getMerchantId(), value.getIpAddress()),
                Grouped.with(merchantIpAddressSerde, cardTransactionSerde))
            // count transactions over a 5 minute window
            .windowedBy(TimeWindows.of(Duration.ofMinutes(5)))
            .count(Materialized.as("MerchantIpAddressCounts"))
            .toStream();
        counts.print(Printed.<Windowed<MerchantIpAddress>, Long>toSysOut().withLabel("counts"));

        final KStream<Windowed<MerchantIpAddress>, MerchantCount> enrichedCounts = counts
            .join(merchants,
                (key, value) -> key.key().getMerchantId(), // join on merchant id
                (count, merchant) -> new MerchantCount(merchant, count));
        enrichedCounts.print(Printed.<Windowed<MerchantIpAddress>, MerchantCount>toSysOut().withLabel("enrichedCounts"));

        final KStream<Windowed<MerchantIpAddress>, MerchantCount> fraudEnrichedCounts = enrichedCounts
            .filter((key, value) -> value.getCount().equals(value.getMerchant().getAlertThreshold()));
        fraudEnrichedCounts.print(Printed.<Windowed<MerchantIpAddress>, MerchantCount>toSysOut().withLabel("fraudEnrichedCounts"));

        final KStream<String, FraudAlert> fraudAlerts = fraudEnrichedCounts //fraudCounts
            .map((key, value) -> {
                final FraudAlert alert =
                    new FraudAlert(
                        UUID.randomUUID().toString(),
                        key.key().getMerchantId(),
                        key.key().getIpAddress());
                return new KeyValue<>(alert.getId(), alert);
            });
        fraudAlerts.print(Printed.<String, FraudAlert>toSysOut().withLabel("fraudAlerts"));

        // write to the result topic, need to override serdes
        fraudAlerts.to(FRAUD_ALERT_TOPIC, Produced.with(stringSerde, fraudAlertSerde));

        final KafkaStreams streams = new KafkaStreams(builder.build(), streamsConfiguration);
        streams.cleanUp();
        streams.start();

        new FraudRestService(streams, new HostInfo("localhost", 7070)).start();

        streams.allMetadataForStore("Merchants").forEach(m -> System.out.println(m.hostInfo()));
    }

}
