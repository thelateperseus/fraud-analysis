package nathan.stream;

import nathan.model.Merchant;

public class MerchantCount {
    private final Merchant merchant;
    private final Long count;

    public MerchantCount(final Merchant merchant, final Long count) {
        this.merchant = merchant;
        this.count = count;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public Long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "MerchantCount [merchant=" + merchant + ", count=" + count + "]";
    }
}
